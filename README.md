# Bitbucket Advanced Pipelines

This repository serves as a small resource of knowledge for deploying large
Bitbucket projects. It employs a custom logic for creation of conditional
pipelines depending on which files or directories were affected on latest merge
or commit to a `master` branch.

## Getting Started

When setting up a fresh bitbucket repository, you'll have to enable pipelines
first. This is a **new commit**, therefore it will have no file changes. It will
however, launch your first pipeline, should you have a `bitbucket-pipelines.yml`
file in your repository already.

## Set up

Instead of defining pipeline steps in the `bitbucket-pipelines.yml`, you will
define them in shell scripts with conditional statements. This means that any
action that must be taken to build/deploy/release etc will depend on certain
conditions.

In this example we create two environment variables that are defined from the
state of files and folders:

- `FRONTEND_CHANGED` - if anything in the folder `client/` has changed
- `BACKEND_CHANGED` - if anything in the folders `cloudformation/`, `cognito/`
or `backend/` changed

The changes are detected at **the last push or merge** to `master` in the `origin`
bitbucket repository. This means that any consecutive local commits to `master`
(until last push) are ignored. Therefore, it is imperative that you make
changes in a separate local branch, push that branch and make a pull request
(merge) with `master` at the `origin` repository. Such a merge will include
**all changes** since last commit to `master`.

The first step in our pipeline will check for file/folder changes and save
the environment variables necessary as artifacts for the next steps. In our
example we only check 4 folders:

```sh
    do
        if [[ "$line" == client* ]]; then CLIENT_CHANGED=1; fi
        if [[ "$line" == backend* ]] \
        || [[ "$line" == cloudformation* ]] \
        || [[ "$line" == cognito* ]]; then BACKEND_CHANGED=1; fi
    done < "$input"
```

This function simply checks the git output of files changed and compares
their starting string with wildcard matches to define our variables.

Consecutive steps will need to read the status and execute their own scripts:

```sh
    source .status/status

    if [ -n "${CLIENT_CHANGED}" ]; then
        echo "client has been changed, running client pipeline..."
        # frontend deployment script
    else
        echo "client unchanged"
    fi
```

As you can see, should there be no changes made to any of the above mentioned
folders, a pipeline will nevertheless by triggered. It will run for mere seconds
and complete with a successful exit.

> **IMPORTANT:** Even if you work alone, commit all local changes to a separate
branch and push that to the `origin`, before merging onto `master`.