source .status/status

if [ -n "${BACKEND_CHANGED}" ]; then
    echo "backend has been changed, running backend pipeline..."
    # backend deployment script
else
    echo "backend unchanged"
fi