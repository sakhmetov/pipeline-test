source .status/status

if [ -n "${CLIENT_CHANGED}" ]; then
    echo "client has been changed, running client pipeline..."
    # frontend deployment script
else
    echo "client unchanged"
fi