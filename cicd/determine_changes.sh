echo "The following files have been changed:"
git log -m -1 --name-only | awk 'NR==3' RS="\n\n"

# reset status
CLIENT_CHANGED=""
BACKEND_CHANGED=""

mkdir -p .status
git log -m -1 --name-only | awk 'NR==3' RS="\n\n" > .status/changes

input=".status/changes"
while IFS= read -r line
do
    if [[ "$line" == client* ]]; then CLIENT_CHANGED=1; fi
    if [[ "$line" == backend* ]] \
    || [[ "$line" == cloudformation* ]] \
    || [[ "$line" == cognito* ]]; then BACKEND_CHANGED=1; fi
done < "$input"

echo "CLIENT_CHANGED=$CLIENT_CHANGED" > .status/status
echo "BACKEND_CHANGED=$BACKEND_CHANGED" >> .status/status